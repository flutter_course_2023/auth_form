import 'package:flutter/material.dart';


class ProductView extends StatelessWidget {

  //final Product product;
  const ProductView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var url = 'https://www.venushomeappliances.com/storage/app/product/09ee25e0-3973-11ec-845e-c529e2402238/20211030111759orega-image-0.jpg';
    return Container(
      width: size.width*0.95,
      height: size.width*0.95,
      margin: EdgeInsets.all(size.width*0.025),
      padding: EdgeInsets.all(size.width*0.025),
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.45),
        borderRadius: BorderRadius.circular(size.width*0.025),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
              'product name',
              style: TextStyle(
                fontSize: size.width*0.05,
                color: Colors.blue[800]
              ),
          ),
          Text(
              'price : 25 \$',
              style: TextStyle(
                fontSize: size.width*0.05
              ),
          ),
          Container(
            width: double.infinity,
            height: size.width*0.45,
            margin: EdgeInsets.symmetric(vertical:size.width*0.05),
            decoration: BoxDecoration(
              color: Colors.blue.withOpacity(0.45),
              borderRadius: BorderRadius.circular(size.width*0.025),
              image: DecorationImage(
                image: NetworkImage(url,),
                fit: BoxFit.cover
              )
            ),

          ),
          Text(
            """description of the product description of the product
description of the product description of the product
description of the product description of the product
description of the product description of the product""",
            style: TextStyle(
                fontSize: size.width*0.0425,
                color: Colors.black87
            ),
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
          ),
        ],
      ),
    );
  }
}
