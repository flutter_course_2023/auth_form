import 'package:flutter/material.dart';

import '../components/product_view.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  static const name = '/home';



  @override
  Widget build(BuildContext context) {
    //List<Product> = [,,,];
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        // leading: IconButton(
        //   icon: Icon(Icons.arrow_back_ios),
        //   onPressed: ()=> Navigator.maybePop(context),
        // ),
      ),
      body: ListView.builder(
          itemCount: 10,
          itemBuilder: (context,i) =>
              ProductView(),
      )
    );
  }
}
