import 'package:auth_form/modules/home/screens/home_screen.dart';
import 'package:auth_form/modules/register/screens/register_screen.dart';
import 'package:flutter/material.dart';

import '../../../core/general_components/app_text_field.dart';


class LoginScreen extends StatelessWidget {

  static const name = '/login';


  LoginScreen({Key? key}) : super(key: key);

  final TextEditingController emailController
    = TextEditingController(
      //text: 'example@gmail.com'
    );

  final TextEditingController passController
    = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(size.width * 0.025),
                  child: CircleAvatar(
                    radius: size.width*0.2,
                    child: Icon(
                      Icons.person_outline,
                      size: size.width*0.3,
                    ),
                  ),
                ),
                Divider(),
                AppTextFormField(
                  controller: emailController,
                  hint: 'email',
                  icon: const Icon(Icons.email),
                  validator: (text){
                    if(text == null || text.isEmpty)
                      return 'email field is required';
                    else if( ! text.contains('@'))
                      return 'value must be a valid email';

                    return null;
                  },
                ),
                AppTextFormField(
                  controller: passController,
                  hint: 'password',
                  icon: const Icon(Icons.lock),
                  isPass: true,
                  validator: (text){
                    if(text == null || text.isEmpty)
                      return 'password field is required';
                    else if(text.length < 6)
                      return 'password must be at least 6 characters';

                    return null;
                  },
                ),
                ElevatedButton(
                    onPressed: (){
                      if(!(_formKey.currentState?.validate() ?? false))
                        return;
                      Navigator.pushReplacementNamed(context, HomeScreen.name);
                    },
                    child: Text('submit')
                ),
                Padding(
                    padding: EdgeInsets.all(0.025*size.width),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('if you don\'t have an account go to '),
                        InkWell(
                          onTap: (){
                            // Navigator.pushReplacement(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) =>
                            //           RegisterScreen()
                            //     )
                            // );
                            Navigator.pushReplacementNamed(context,RegisterScreen.name);
                          },
                          child: Text(
                              'register',
                            style: TextStyle(
                              color: Colors.indigo,
                              fontWeight: FontWeight.w600
                            ),
                          ),
                        ),
                      ],
                    ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
