import 'package:auth_form/modules/home/screens/home_screen.dart';
import 'package:auth_form/modules/login/screens/login_screen.dart';
import 'package:flutter/material.dart';

import '../../../core/general_components/app_text_field.dart';

class RegisterScreen extends StatelessWidget {

  static const name = '/register';

  RegisterScreen({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(0.025*size.width),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('if you already have an account go to '),
                    InkWell(
                      onTap: (){
                        // Navigator.pushReplacement(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) =>
                        //             LoginScreen()
                        //     )
                        // );
                        Navigator.pushReplacementNamed(context, LoginScreen.name);
                      },
                      child: Text(
                        'login',
                        style: TextStyle(
                            color: Colors.indigo,
                            fontWeight: FontWeight.w600
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
