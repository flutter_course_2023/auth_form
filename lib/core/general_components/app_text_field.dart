import 'package:flutter/material.dart';

class AppTextFormField extends StatefulWidget {
  final TextEditingController? controller;
  final String? hint;
  final Widget? icon;
  final bool isPass;
  final FormFieldValidator<String>? validator;

  const AppTextFormField({
    Key? key,
    this.controller,
    this.hint,
    this.icon,
    this.isPass = false,
    this.validator
  }) : super(key: key);

  @override
  State<AppTextFormField> createState() => _AppTextFormFieldState();
}

class _AppTextFormFieldState extends State<AppTextFormField> {

  bool isHiden = true;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(0.025*size.width),
      child: TextFormField(
        controller: widget.controller,
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(0.025*size.width)
            ),
            hintText: widget.hint,
            prefixIcon: widget.icon,
            suffixIcon: Visibility(
              visible: widget.isPass,
              child: IconButton(
                icon: Icon(
                    isHiden ? Icons.visibility_off
                            : Icons.visibility
                ),
                onPressed: (){
                  setState(() {
                    isHiden = !isHiden;
                  });
                },
              ),
            ),
            fillColor: Colors.grey.withOpacity(0.5),
            filled: true
        ),
        obscureText: widget.isPass && isHiden,
        validator: widget.validator,
      ),
    );
  }
}
