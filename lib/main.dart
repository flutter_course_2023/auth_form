import 'package:auth_form/modules/home/screens/home_screen.dart';
import 'package:auth_form/modules/register/screens/register_screen.dart';
import 'package:flutter/material.dart';
import 'modules/login/screens/login_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: LoginScreen.name,
      routes: {
        LoginScreen.name:(c) => LoginScreen(),
        HomeScreen.name:(c) => HomeScreen(),
        RegisterScreen.name:(c) => RegisterScreen(),
      }
    );
  }
}
